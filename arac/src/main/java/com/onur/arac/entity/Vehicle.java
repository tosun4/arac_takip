package com.onur.arac.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.util.Date;

@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String resmi_plaka;
    private String sivil_plaka;
    private String model;
    private String sicil;
    private String km_giris;
    private String ikamet;
    private String bakim;
    private Long total_km;
    private Date son_tarih;
    private Long sonraki_bakim;

    public Long getBakima_kalan_km() {
        return bakima_kalan_km;
    }

    public void setBakima_kalan_km(Long bakima_kalan_km) {
        this.bakima_kalan_km = bakima_kalan_km;
    }

    private Long bakima_kalan_km;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResmi_plaka() {
        return resmi_plaka;
    }

    public void setResmi_plaka(String resmi_plaka) {
        this.resmi_plaka = resmi_plaka;
    }

    public String getSivil_plaka() {
        return sivil_plaka;
    }

    public void setSivil_plaka(String sivil_plaka) {
        this.sivil_plaka = sivil_plaka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSicil() {
        return sicil;
    }

    public void setSicil(String sicil) {
        this.sicil = sicil;
    }

    public String getKm_giris() {
        return km_giris;
    }

    public void setKm_giris(String km_giris) {
        this.km_giris = km_giris;
    }

    public String getIkamet() {
        return ikamet;
    }

    public void setIkamet(String ikamet) {
        this.ikamet = ikamet;
    }

    public String getBakim() {
        return bakim;
    }

    public void setBakim(String bakim) {
        this.bakim = bakim;
    }

    public Long getTotal_km() {
        return total_km;
    }

    public void setTotal_km(Long total_km) {
        this.total_km = total_km;
    }

    public Date getSon_tarih() {
        return son_tarih;
    }

    public void setSon_tarih(Date son_tarih) {
        this.son_tarih = son_tarih;
    }

    public Long getSonraki_bakim() {
        return sonraki_bakim;
    }

    public void setSonraki_bakim(Long sonraki_bakim) {
        this.sonraki_bakim = sonraki_bakim;
    }

    @PrePersist
    protected void onCreate() {
        this.son_tarih = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        this.son_tarih = new Date();
    }
}
